# common initialization

on early-init
    # RAM boost 2.0
    setprop ro.config.moto_swap_supported true
    setprop ro.config.use_common_max_apps true

on post-fs
    # LMK
    setprop ro.lmk.kill_heaviest_task true
    setprop ro.lmk.pgscan_limit 3000
    setprop ro.lmk.swap_free_low_percentage 10
    setprop ro.lmk.swap_util_max 90
    setprop ro.lmk.thrashing_limit 50
    setprop ro.lmk.thrashing_limit_decay 25
    setprop ro.lmk.thrashing_limit_critical 50
    setprop ro.lmk.min_thrashing_limit 10
    setprop ro.lmk.threshold_decay 50
    setprop ro.lmk.psi_complete_stall_ms 150
    setprop ro.lmk.psi_partial_stall_ms 50
    setprop ro.lmk.file_high_percentage 70
    setprop ro.lmk.file_low_percentage 25
    setprop ro.lmk.filecache_min_kb 300000
    setprop persist.lmk.debug true
    setprop ro.lmk.kill_timeout_ms 100

    #LMK 3.1
    setprop ro.lmk.kswapd_limit 90
    setprop ro.lmk.kswapd_limit_decay 10
    setprop ro.lmk.kswapd_min_adj 201
    setprop ro.lmk.use_moto_strategy true

    # Promote recent UI procs
    setprop persist.sys.aitune_promote_recent_ui_procs true
    setprop persist.sys.aitune_promote_important_apps true

    # use psi avg10 for mempressure in fwk to avoid ping-pong.
    setprop ro.config.use_psi_avg10_for_mempressure true

    # delay longer for service restart, will be rescheduled immediately once mempressure backing to normal.
    setprop ro.config.svc_restart_delay_on_moderate_mem 3600000
    setprop ro.config.svc_restart_delay_on_low_mem 3600000
    setprop ro.config.svc_restart_delay_on_critical_mem 3600000

    # Psi strategy
    setprop ro.lmk.stall_limit_medium 1
    setprop ro.lmk.stall_limit_critical 4
    setprop ro.lmk.stall_limit_freeze 8
    setprop ro.lmk.medium_min_adj 920
    setprop ro.lmk.critical_min_adj 201
    setprop ro.lmk.freeze_min_adj 0

    # App compactor
    setprop ro.config.use_compaction true
    setprop ro.config.compact_action_1 4
    setprop ro.config.compact_action_2 2
    setprop ro.config.compact_procstate_throttle 11,18
    #Pin kcompactd0 to litte core
    setprop ro.lmk.always_bg_cpuset_threads kcompactd0

    # LowMemoryDetector of Framework
    setprop ro.lowmemdetector.psi_low_stall_us 100000
    setprop ro.lowmemdetector.psi_medium_stall_us 150000
    setprop ro.lowmemdetector.psi_high_stall_us 200000
    setprop persist.sys.perf_fwk_enabled true

# ANR timeout
on post-fs && property:ro.build.type=userdebug
    setprop persist.sys.dispatch_timeout_multiplier 2
    setprop persist.sys.service_timeout 40000
    setprop persist.sys.service_bg_timeout 400000
    setprop persist.sys.bind_timeout 36000

# Disable usap Process pool
on property:persist.device_config.runtime_native.usap_pool_enabled=true
    setprop persist.device_config.runtime_native.usap_pool_enabled false

on property:sys.boot_completed=1 && property:ro.vendor.zram.product_swapon=true
    trigger sys-boot-completed-set

on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=false
    swapon_all /vendor/etc/fstab.qcom.zram
on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=true
    swapon_all /vendor/etc/fstab.qcom.zramwb

on property:ro.product.cpu.abi=arm64-v8a
    setprop dalvik.vm.dex2oat64.enabled true

on boot
    #MotoBtAptxMode
    setprop persist.mot_bt.qss_cert true
    #bt stack
    setprop persist.vendor.btstack.qhs_enable true
    #aptX lossless
    setprop persist.mot_bt.lossless_cert true
    #LHDC
    setprop persist.mot_bt.lhdc_enable true
    #LE audio: enable LE audio unicast/broadcast features
    setprop persist.vendor.service.bt.adv_audio_mask 7

on property:sys.boot_completed=1
    # always reserve one CPU for top app
    write /dev/cpuset/top-app/cpus 0-7
    # for SF boost
    write /dev/cpuset/boost-app/cpus 0-6
    write /dev/cpuset/foreground/cpus 0-6
    write /dev/cpuset/system-background/cpus 0-3
    # restrict tasks to little core in screen off for power saving
    write /dev/cpuset/restricted/cpus 0-3
    # restrict bg tasks
    write /dev/cpuset/background/cpus 0-2
